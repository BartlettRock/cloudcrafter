<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Schematic;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('account.files');
    }

    public function drive()
    {
        $schematics = \Auth::user()->schematics()->get();
        return view('schematic.index', compact('schematics'));
    }

    /**
     * Adds schematic to drive.
     *
     * @param  int  $id
     * @return Response
     */
    public function addToDrive($id)
    {
        // $comment = new Comment(array('message' => 'A new comment.'));
        // $post = Post::find(1);
        // $comment = $post->comments()->save($comment);
        $schematic = Schematic::where('schematic_id', $id)->get()->first();
        if($schematic == null)return 'schematic not found';
        \Auth::user()->schematics()->save($schematic);
        return Redirect::action('AccountController@drive');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
