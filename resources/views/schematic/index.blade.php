@extends('app')

@section('styles')
<link href="{{ asset('/css/view.css') }}" rel="stylesheet">
@endsection

@section('side-nav')
<ul>
    <li class="active"><a href>Files</a></li>
    <li><a href>Stats</a></li>
    <li><a href>Settings</a></li>
</ul>
@endsection

@section('content')
<form action="add" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="file" name="filefield">
        <input type="submit">
    </form>

 <h1>Drive</h1>

 <div class="row">

    <ul>
     @foreach($schematics as $schematic)
        <li>id: {{$schematic->schematic_id}}, filename: {{$schematic->filename}}</li>
     @endforeach
    </ul>

 </div>
@endsection
